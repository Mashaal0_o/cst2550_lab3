#include <iostream>
using namespace std;

int main()

{
  float num1, num2, sum, difference, average, product, distance;

  cout << "Enter two numbers: " ;
  cin >> num1 >> num2;

  sum = num1 + num2 ;
  difference = num1 - num2;
  average = sum/2;
  product = num1 * num2 ;
  distance = abs(difference) ;

  cout << "the sum is " << sum;
  cout << "\nthe difference is " << difference;
  cout << "\nthe average is " << average;
  cout << "\nthe product is " << product ;
  cout << "\nthe distance is " << distance;

  return 0;
}
